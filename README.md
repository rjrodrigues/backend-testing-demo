### Project setup
https://start.spring.io/#!type=gradle-project&language=java&platformVersion=3.0.2&packaging=jar&jvmVersion=17&groupId=com.ruby&artifactId=testing&name=testing&description=Demo%20project%20for%20Spring%20Boot&packageName=com.ruby.testing&dependencies=native,devtools,lombok,web,postgresql,data-jpa

### Docker database
docker run --name postgres --rm -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=root -e PGDATA=/var/lib/postgresql/data/pgdata -v /tmp:/var/lib/postgresql/data -p 5432:5432 -it postgres:14.1-alpine

### Swagger
http://localhost:8080/api/swagger