package com.ruby.backup.controller;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.ruby.backup.wiremock.WireMockConfig;
import com.ruby.backup.wiremock.mock.QuoteMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration(classes = { WireMockConfig.class })
class QuoteControllerTest {
    @Autowired
    private WireMockServer wireMockServer;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setup() throws IOException {
        QuoteMock.setupMockQuotesResponse(wireMockServer);
    }

    @Test
    void shouldWork() throws Exception {
        var result = this.mockMvc.perform(get("/api/quotes")).andExpect(status().is2xxSuccessful()).andReturn();
        String content = result.getResponse().getContentAsString();

        assertEquals("[{\"text\":\"Just do it!\",\"author\":\"Nike\"}]", content);
    }
}