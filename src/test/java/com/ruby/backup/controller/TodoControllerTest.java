package com.ruby.backup.controller;

import com.ruby.backup.entity.Todo;
import com.ruby.backup.repository.TodoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TodoControllerTest {
    @Autowired
    private TodoRepository todoRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldWork() throws Exception {
        var todo = new Todo();
        todo.setTitle("title demo");
        todoRepository.save(todo);

        var result = this.mockMvc.perform(get("/api/todos")).andExpect(status().is2xxSuccessful()).andReturn();
        String content = result.getResponse().getContentAsString();
        assertEquals("[{\"id\":1,\"title\":\"title demo\"}]", content);
    }
}