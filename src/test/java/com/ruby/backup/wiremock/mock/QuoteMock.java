package com.ruby.backup.wiremock.mock;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.io.IOException;

import static java.nio.charset.Charset.defaultCharset;
import static org.springframework.util.StreamUtils.copyToString;

public class QuoteMock {
    public static void setupMockQuotesResponse(WireMockServer wireMockServer) throws IOException {
        wireMockServer.stubFor(WireMock.get(WireMock.urlEqualTo("/api/quotes"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                        .withBody(
                                copyToString(
                                        QuoteMock.class.getClassLoader().getResourceAsStream("payload/get-quotes-response.json"),
                                        defaultCharset()))));
    }
}
