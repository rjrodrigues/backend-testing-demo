package com.ruby.backup.converter;

import com.ruby.backup.entity.Todo;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TodoConverterTest {
    @Test
    void shouldConvertEntityToDto() {
        var todo = new Todo();
        todo.setId(1L);
        todo.setTitle("Write unit tests!");

        var todoConverter = new TodoConverter();
        var todoDto = todoConverter.convertEntityToDto(todo);
        assertEquals("Write unit tests!", todoDto.getTitle());
    }
}