package com.ruby.backup.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TodoDto {
    private Long id;
    private String title;
}
