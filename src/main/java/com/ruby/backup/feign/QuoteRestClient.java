package com.ruby.backup.feign;

import com.ruby.backup.model.Quote;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(value = "quote", url = "http://localhost:3000")
public interface QuoteRestClient {
    @GetMapping(value = "/api/quotes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    List<Quote> getQuotes();
}
