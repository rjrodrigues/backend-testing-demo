package com.ruby.backup.controller;

import com.ruby.backup.converter.QuoteConverter;
import com.ruby.backup.dto.QuoteDto;
import com.ruby.backup.service.QuoteService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/quotes")
public class QuoteController {

    private final QuoteService quoteService;
    private final QuoteConverter quoteConverter;

    public QuoteController(QuoteService quoteService, QuoteConverter quoteConverter) {
        this.quoteService = quoteService;
        this.quoteConverter = quoteConverter;
    }

    @GetMapping
    List<QuoteDto> getAll() {
        return this.quoteService.getAll().stream().map(this.quoteConverter::convertModelToDto).collect(Collectors.toList());
    }
}
