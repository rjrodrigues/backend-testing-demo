package com.ruby.backup.controller;

import com.ruby.backup.converter.TodoConverter;
import com.ruby.backup.dto.TodoDto;
import com.ruby.backup.service.TodoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/todos")
public class TodoController {

    private final TodoService todoService;
    private final TodoConverter todoConverter;

    public TodoController(TodoService todoService, TodoConverter todoConverter) {
        this.todoService = todoService;
        this.todoConverter = todoConverter;
    }

    @GetMapping
    List<TodoDto> getAll() {
        return this.todoService.getAll().stream().map(this.todoConverter::convertEntityToDto).collect(Collectors.toList());
    }
}
