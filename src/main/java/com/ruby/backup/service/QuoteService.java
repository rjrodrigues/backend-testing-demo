package com.ruby.backup.service;

import com.ruby.backup.feign.QuoteRestClient;
import com.ruby.backup.model.Quote;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuoteService {
    private final QuoteRestClient quoteRestClient;

    public QuoteService(QuoteRestClient quoteRestClient) {
        this.quoteRestClient = quoteRestClient;
    }

    public List<Quote> getAll() {
        return this.quoteRestClient.getQuotes();
    }
}
