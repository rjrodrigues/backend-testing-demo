package com.ruby.backup.model;

import lombok.Data;

@Data
public class Quote {
    private String text;
    private String author;
}
