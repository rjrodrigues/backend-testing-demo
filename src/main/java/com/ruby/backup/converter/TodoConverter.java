package com.ruby.backup.converter;

import com.ruby.backup.dto.TodoDto;
import com.ruby.backup.entity.Todo;
import org.springframework.stereotype.Service;

@Service
public class TodoConverter {

    public TodoDto convertEntityToDto(Todo todo) {
        return new TodoDto(todo.getId(), todo.getTitle());
    }

}
