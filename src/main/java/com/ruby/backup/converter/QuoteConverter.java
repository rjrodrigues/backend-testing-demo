package com.ruby.backup.converter;

import com.ruby.backup.dto.QuoteDto;
import com.ruby.backup.model.Quote;
import org.springframework.stereotype.Service;

@Service
public class QuoteConverter {
    public QuoteDto convertModelToDto(Quote quote) {
        var quoteDto = new QuoteDto();
        quoteDto.setAuthor(quote.getAuthor());
        quoteDto.setText(quote.getText());
        return quoteDto;
    }
}
