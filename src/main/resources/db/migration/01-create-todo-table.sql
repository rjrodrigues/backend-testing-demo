create table todo (
    id bigserial primary key,
    title varchar(50) not null,
    created_time timestamp not null default now()
)